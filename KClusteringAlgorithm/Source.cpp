#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <climits>

using namespace std;

class GraphNode{
public:
	int node1;
	int node2;
	int edgeCost;

	GraphNode(int n1, int n2, int ec){
		node1 = n1;
		node2 = n2;
		edgeCost = ec;
	}
};

class UnionFind{
private:
	vector<int> id;
	vector<int> rankOfIds;
	int size;

public:
	UnionFind(int N){
		id.resize(N);
		rankOfIds.resize(N);
		size = N;
		for(int i=0; i<N; i++){
			id[i] = i;
			rankOfIds[i] = 1;
		}
	}

	int find(int p){
		return id[p];
	}

	int doUnion(int p, int q){
		if(rankOfIds[id[p]] <= rankOfIds[id[q]]){
			int temp = id[p];
			for(int i=0; i<size; i++){
				if(id[i] == temp){
					id[i] = id[q];
					rankOfIds[id[q]]++;
				}
			}
			return temp;
		}else{
			int temp = id[q];
			for(int i=0; i<size; i++){
				if(id[i] == temp){
					id[i] = id[p];
					rankOfIds[id[p]]++;
				}
			}
			return temp;
		}
	}

	bool isConnected(int p, int q){
		return id[p] == id[q];
	}
};

class Graph{
private:
	vector<GraphNode> edges;
	vector< vector<int> > adjacencyMatrix;
	void sortNodesByCost();
	int number_of_nodes;
	UnionFind *uf;

public:
	int maximum_spacing;
	Graph(fstream &myfile);
	GraphNode getEdge(int i);
	void eraseLeader(vector<int> &vector1, int index);
};

bool sortingFunction(GraphNode& node1, GraphNode& node2){
	if(node1.edgeCost < node2.edgeCost){
		return true;
	}
	return false;
}

void Graph::sortNodesByCost(){
	sort(edges.begin(), edges.end(), sortingFunction);
}

void Graph::eraseLeader(vector<int> &vector1, int index){
	for(int i=0; i<vector1.size(); i++){
		if(vector1[i] == index){
			vector1.erase(vector1.begin()+i);
			break;
		}
	}
}

Graph::Graph(fstream &myfile){
	myfile.seekg(0);
	myfile >> number_of_nodes;
	adjacencyMatrix.resize(number_of_nodes);
	for(int i=0; i<number_of_nodes; i++){
		adjacencyMatrix[i].resize(number_of_nodes);
	}

	for(int i=0; i<number_of_nodes; i++){
		for(int j=0; j<number_of_nodes; j++){
			adjacencyMatrix[i][j] = INT_MAX;
		}
	}

	while(myfile.good()){
		int node1, node2, edgeCost;
		myfile >> node1 >> node2 >> edgeCost;
		edges.push_back(GraphNode(node1, node2, edgeCost));
		adjacencyMatrix[node1-1][node2-1] = edgeCost;
		adjacencyMatrix[node2-1][node1-1] = edgeCost;
	}

	sortNodesByCost();
	uf = new UnionFind(number_of_nodes);

	vector<int> leaders;

	for(int i=0; i<number_of_nodes; i++){
		leaders.push_back(i);
	}

	int index=0;
	while(leaders.size() != 4){
		//cout<<index<<endl;
		if(uf->isConnected(edges[index].node1-1, edges[index].node2-1)){
			//do nothing
		}else{
			int id = uf->doUnion(edges[index].node1-1, edges[index].node2-1);
			eraseLeader(leaders, id);
			//cout<<id<<endl;
			//cout<<leaders.size()<<endl<<endl;
		}
		index++;
	}

	for(int i=0; i<leaders.size(); i++){
		cout<<leaders[i]<<endl;
	}

	int min = INT_MAX;
	for(int i=0; i<number_of_nodes; i++){
		for(int j=0; j<number_of_nodes; j++){
			if(uf->find(i) != uf->find(j)){
				if(min > adjacencyMatrix[i][j]){
					min = adjacencyMatrix[i][j];
				}
			}
		}
	}

	maximum_spacing = min;
}

GraphNode Graph::getEdge(int i){
	return edges[i];
}

int main(int argc, char *argv[]){
	fstream myfile;
	myfile.open("clustering1.txt", ios::in);
	Graph graph(myfile);

	cout<<endl<<"The maximum spacing is : "<<graph.maximum_spacing<<endl;
}